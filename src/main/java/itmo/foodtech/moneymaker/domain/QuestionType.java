package itmo.foodtech.moneymaker.domain;

public enum QuestionType {
    DROPDOWN,
    TEXTAREA,
    CHECKBOX,
    MULTIPLE_CHOICE
}
